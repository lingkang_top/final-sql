# 使用UI生成
基于java fx，实用jdk8，jdk8+可以参考文章：
https://blog.csdn.net/weixin_44480167/article/details/126362044

## 生成
```java
import top.lingkang.finalsql.ui.GenerateUI;

/**
 * @author lingkang
 * Created by 2022/11/29
 */
public class UiBuild {
    public static void main(String[] args) {
        GenerateUI.main(args);
    }
}
```
![https://gitee.com/lingkang_top/final-sql/raw/master/doc/gui.png](gui.png)