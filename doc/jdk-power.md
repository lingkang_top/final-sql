# JDK各版本反射权限问题及解决
## jdk17
如果出现反射权限问题。可添加jvm参数：
```html
--add-opens java.base/java.lang=ALL-UNNAMED （取消了 illegal-access 参数）
```

## jdk12 - jdk16
如果出现反射权限问题。可添加jvm参数：
```html
--illegal-access=permit （默认为 deny ）
```

#示例：
## jdk9 - jdk11
没有反射权限问题 。默认为 
```html
--illegal-access=permit
```