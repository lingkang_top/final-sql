## v3.1.3-SNAPSHOT 发布
#### 修复bug，添加UI，发布一个快照
> 2023年1月12日
```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-sql-core</artifactId>
    <version>3.1.3-SNAPSHOT</version>
</dependency>

<!--快照仓库-->
<repositories>
    <repository>
        <id>nexus</id>
        <name>Nexus</name>
        <layout>default</layout>
        <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
        <releases>
            <enabled>true</enabled>
        </releases>
    </repository>
</repositories>
```

修复查询一个结果时无法识别驼峰列属性，oneResult，v3.1.3

## v3.1.2 发布
#### 完善GUI，增加sqlite支持
> 2022年12月2日
```java
public class UiBuild {
    public static void main(String[] args) {
        GenerateUI.main(args);
    }
}
```

## v3.1.1 发布
#### 完善GUI
> 2022年11月29日
```java
public class UiBuild {
    public static void main(String[] args) {
        GenerateUI.main(args);
    }
}
```

## v3.1.0 发布
#### 修复bug
> 2022年11月12日

* 1、修复查询entity对象无法获取到驼峰命名列
* 2、修复更新条件入参错误
* 3、添加ui生成
* 4、添加原生更新列自动转化驼峰


## v3.0.1 发布
#### 修复bug
> 2022年11月5日

修复自定义列名bug

## v3.0.0 发布
#### 增加 mybatis xml解析
> 2022年10月11日


## v2.2.0 发布
#### 增加spring下的事务管理
* 增加spring下的事务管理，配置 **FinalSqlSpring5TransactionManagement** 后，可通过 **@Transactional** 管理事务
> 2022年10月8日


## v2.1.0 发布
#### 修复分页类型转换异常
* 修复：修复分页类型转换异常；
* 丰富数据类型处理；
* 对内部类进行了闭包；
> 2022年9月30日

<br><br>
**2.0.x存在分页处理异常**


## v2.0.3 发布
#### 修复驼峰结果为空问题
修复：映射实体为驼峰命名：aA，数据库为：a_a，映射列名错误导致结果为空。
> 2022年9月26日


## v2.0.2 发布
#### 修复驼峰结果为空问题
修复：映射实体为驼峰命名：aA，数据库为：a_a，映射列名错误导致结果为空。
> 2022年9月26日


## v2.0.1 发布
#### 修复响应结果处理BUG
当映射实体类存在非标列映射属性参数时（与表无关的多余属性变量），查询返回结果处理异常。**lingkang已修复2022-09-25**
> 2022年9月25日 18:31:53