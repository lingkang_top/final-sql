## 文件流操作

### byte[]
```java
@Getter
@Setter
@Table("f_file")
public class FileByte implements Serializable {
    @Id
    private Integer id;
    @Column
    private String name;
    @Column
    private byte[] content;
}
```
插入和读取
```java
    @Test
    public void insert() throws Exception {
        File file = new File(FileBlobMysqlTest.class.getClassLoader().getResource("init-mysql.sql").getFile());
        FileByte fileByte = new FileByte();
        fileByte.setName("file-byte");
        byte[] bytes = new byte[(int) file.length()];
        new FileInputStream(file).read(bytes);
        fileByte.setContent(bytes);
        finalSql.insert(fileByte);
    }

    @Test
    public void read() throws Exception {
        File file = new File("d://final-sql-temp-file-byte.sql");
        if (file.exists())
            file.delete();

        FileByte fileByte = finalSql.selectOne(FileByte.class, new Condition().eq("name", "file-byte"));
        FileOutputStream out = new FileOutputStream(file);
        out.write(fileByte.getContent());
        out.flush();
        out.close();
        System.out.println("success!");
    }
```

### Blob
```java
import lombok.Getter;
import lombok.Setter;
import top.lingkang.finalsql.annotation.Column;
import top.lingkang.finalsql.annotation.Id;
import top.lingkang.finalsql.annotation.Table;

import java.io.Serializable;
import java.sql.Blob;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
@Getter
@Setter
@Table("f_file")
public class FileBlob implements Serializable {
    @Id
    private Integer id;
    @Column
    private String name;
    @Column
    private Blob content;
}
```
读写
```java
import com.mysql.cj.jdbc.Blob;
import mysql.common.BaseMysqlTest;
import mysql.entity.FileBlob;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class FileBlobMysqlTest extends BaseMysqlTest {
    @Test
    public void insert()throws Exception{
        File file=new File(FileBlobMysqlTest.class.getClassLoader().getResource("init-mysql.sql").getFile());
        FileBlob fileBlob =new FileBlob();
        fileBlob.setName("123");
        byte[] bytes=new byte[(int) file.length()];
        new FileInputStream(file).read(bytes);
        Blob blob=new Blob(bytes,null);
        fileBlob.setContent(blob);
        finalSql.insert(fileBlob);
    }

    @Test
    public void select()throws Exception{
        FileBlob fileBlob = finalSql.selectOne(FileBlob.class);
        java.sql.Blob content = fileBlob.getContent();

        File file=new File("d://final-sql-temp.sql");
        if (file.exists())
            file.delete();

        FileOutputStream out = new FileOutputStream(file);
        byte[] by=new byte[(int) content.length()];
        content.getBinaryStream().read(by);
        out.write(by);
        out.flush();

        out.close();
        content.getBinaryStream().close();
    }
}
```