package ui;

import top.lingkang.finalsql.ui.GenerateUI;

/**
 * @author lingkang
 * Created by 2022/11/8
 */
public class UIApp {
    public static void main(String[] args) {
        GenerateUI.main(args);
    }
}
