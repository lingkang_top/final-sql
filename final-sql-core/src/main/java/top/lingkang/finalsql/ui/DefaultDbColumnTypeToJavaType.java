package top.lingkang.finalsql.ui;

import top.lingkang.finalsql.ui.DbColumnTypeToJavaType;
import top.lingkang.finalsql.ui.DbToJava;

/**
 * @author lingkang
 * Created by 2022/11/9
 */
class DefaultDbColumnTypeToJavaType implements DbColumnTypeToJavaType {
    @Override
    public DbToJava match(String columnName, String columnType, String dbType) {
        DbToJava column=new DbToJava();
        if ("int".equals(columnType) || "integer".equals(columnType) || "tinyint".equals(columnType)) {
            column.setType("Integer");
        } else if ("varchar".equals(columnType) || "char".equals(columnType) || "json".equals(columnType)
                || columnType.contains("text")) {
            column.setType("String");
        } else if ("float".equals(columnType)) {
            column.setType("Float");
            column.setImportName("import java.lang.Float;");
        } else if ("double".equals(columnType)||"real".equals(columnType)) {
            column.setType("Double");
            column.setImportName("import java.lang.Double;");
        } else if ("decimal".equals(columnType)) {
            column.setType("BigDecimal");
            column.setImportName("import java.math.BigDecimal;");
        } else if ("date".equals(columnType) || columnType.contains("time") || "year".equals(columnType)) {
            column.setType("Date");
            column.setImportName("import java.util.Date;");
        } else if (columnType.contains("blob")) {
            column.setType("byte[]");
        } else if ("bigint".equals(columnType)) {
            column.setType("Long");
        }
        else {
            column.setType("Object");
        }
        return column;
    }
}
