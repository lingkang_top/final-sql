package top.lingkang.finalsql.ui;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 * @author lingkang
 * Created by 2022/9/11
 */
class ToastUtil {
    //默认3秒
    public static void toast(String msg, Window window) {
        toast(msg, 3000, window);
    }

    public static void toast(String msg, int time, Window window) {
        if (msg == null || msg.length() == 0) {
            throw new RuntimeException("msg入参不能为空！");
        }
        Stage stage = new Stage();
        stage.initStyle(StageStyle.TRANSPARENT);//舞台透明
        stage.setResizable(false);
        stage.initModality(Modality.NONE);
        stage.initOwner(window);

        Label label = new Label(msg);//默认信息
        label.setStyle("-fx-background: rgba(56,56,56,0.6);-fx-border-radius: 25;-fx-background-radius: 20");//label透明,圆角
        label.setTextFill(Color.valueOf("white"));//消息字体颜色
        label.setPrefHeight(44);
        label.setPadding(new Insets(15));
        label.setAlignment(Pos.CENTER);//居中
        label.setFont(new Font(16));//字体大小
        Scene scene = new Scene(label);
        scene.setFill(Color.TRANSPARENT);//场景透明
        stage.setScene(scene);

        // 位置
//        System.out.println(window.getX());
//        System.out.println(window.getWidth());
        double x = window.getX() + window.getWidth() / 2;
        double y = window.getY() + window.getHeight() / 2;
        stage.setX(x - 16 * msg.length() / 2);
        stage.setY(y - 22);

        stage.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(time);
                } catch (InterruptedException e) {
                }
                Platform.runLater(() -> {
                    stage.close();
                });
            }
        }).start();
    }
}
