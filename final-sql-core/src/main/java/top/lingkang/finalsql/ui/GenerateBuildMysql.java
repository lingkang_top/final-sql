package top.lingkang.finalsql.ui;

import top.lingkang.finalsql.sql.FinalSql;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author lingkang
 * Created by 2022/11/8
 * mysql 5.7.x
 */
public class GenerateBuildMysql implements GenerateBuild {
    // 获取所有表
    private final String sql_tables = "select TABLE_NAME from information_schema.TABLES where TABLE_SCHEMA=(select database());";
    private final String sql_column = "select COLUMN_NAME,DATA_TYPE,COLUMN_COMMENT,COLUMN_KEY from information_schema.COLUMNS where TABLE_SCHEMA = (select database()) and TABLE_NAME=?;";

    @Override
    public void build(FinalSql finalSql, GenerateProperties properties) {
        GenerateUtils.checkGenerateProperties(properties);
        List<String> tables = finalSql.selectForList(sql_tables, String.class);
        if (tables.isEmpty())
            return;

        String pageName = GenerateUtils.getPage(properties.getOutDir());
        String template = GenerateUtils.readFile(
                GenerateBuildMysql.class.getClassLoader().getResourceAsStream("java-template.txt")
        );
        if (!GenerateUtils.isBlank(pageName)) {
            pageName = "package " + pageName + ";";
        }
        template = template.replace("#package", pageName);
        String ignoreTablePrefix = properties.getIgnoreTablePrefix();
        String ignoreTables = properties.getIgnoreTable();
        List<String> ignoreTable = new ArrayList<>();
        if (ignoreTables != null) {
            String[] s = ignoreTables.replaceAll(" ", "").split(",");
            for (String str : s) {
                ignoreTable.add(str.replaceAll("\\*", "[0-9a-zA-Z_]{0,}"));
            }
        }

        HashSet<String> hasClassName = new HashSet<>();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        for (String table : tables) {
            if (!ignoreTable.isEmpty()) {
                boolean jump = false;
                for (String str : ignoreTable)
                    if (Pattern.matches(str, table)) {
                        jump = true;
                        break;
                    }
                if (jump)
                    continue;
            }

            String className = GenerateUtils.handlerTableName(table, ignoreTablePrefix);
            if (hasClassName.contains(className)) {// 防止相同类型名冲突
                className = GenerateUtils.hasSomeName(className, hasClassName);
            }
            hasClassName.add(className);

            String t = template;
            t = t.replace("#date", date);
            t = t.replace("#table", table);
            t = t.replace("#className", className);

            HashSet<String> impl = new HashSet<>();
            // 列
            List<Map> columns = finalSql.selectForList(sql_column, Map.class, table);
            int keyNumber = 0;
            List<GenerateColumn> generateColumns = new ArrayList<>();
            for (Map<String, String> map : columns) {
                String column_name = map.get("COLUMN_NAME");
                String data_type = map.get("DATA_TYPE");
                String column_comment = map.get("COLUMN_COMMENT");
                String column_key = map.get("COLUMN_KEY");

                GenerateColumn column = GenerateUtils.columnTypeToJavaType(column_name, data_type, "mysql");
                column.setComment(column_comment);
                if (!GenerateUtils.isBlank(column_key) && column_key.toLowerCase().contains("pri")) {
                    keyNumber++;
                    column.setPri(true);
                }

                generateColumns.add(column);

                if (column.getImportName() != null) {
                    impl.add(column.getImportName());
                }
            }
            String imports = "";
            for (String im : impl) {
                imports = imports + im + "\n";
            }
            t = t.replace("#import", imports);

            String entity = GenerateUtils.columnToString(generateColumns, keyNumber);
            t = t.replace("#entity", entity);

            System.out.println(t);

            // out java file
            outFile(
                    properties.getOutDir() + File.separator + className + ".java",
                    t,
                    properties.isCover()
            );
        }
    }
}
