package top.lingkang.finalsql.ui;

/**
 * @author lingkang
 * Created by 2022/11/9
 */
class DbToJava {
    private String type;
    private String importName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImportName() {
        return importName;
    }

    public void setImportName(String importName) {
        this.importName = importName;
    }

    @Override
    public String toString() {
        return "DbToJava{" +
                "type='" + type + '\'' +
                ", importName='" + importName + '\'' +
                '}';
    }
}
