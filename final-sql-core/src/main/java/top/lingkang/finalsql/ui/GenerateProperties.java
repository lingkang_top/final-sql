package top.lingkang.finalsql.ui;

/**
 * @author lingkang
 * Created by 2022/11/8
 */
public class GenerateProperties {
    // 输出java entity 的目录
    private String outDir;
    // 存在了是否覆盖
    private boolean cover;
    // 忽略的表前缀
    private String ignoreTablePrefix;
    // 忽略的表
    private  String ignoreTable;

    public String getIgnoreTable() {
        return ignoreTable;
    }

    public void setIgnoreTable(String ignoreTable) {
        this.ignoreTable = ignoreTable;
    }

    public String getIgnoreTablePrefix() {
        return ignoreTablePrefix;
    }

    public void setIgnoreTablePrefix(String ignoreTablePrefix) {
        this.ignoreTablePrefix = ignoreTablePrefix;
    }

    public boolean isCover() {
        return cover;
    }

    public void setCover(boolean cover) {
        this.cover = cover;
    }

    public String getOutDir() {
        return outDir;
    }

    public void setOutDir(String outDir) {
        this.outDir = outDir;
    }
}
