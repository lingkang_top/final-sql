package top.lingkang.finalsql.ui;

/**
 * @author lingkang
 * Created by 2022/11/9
 */
interface DbColumnTypeToJavaType {
    DbToJava match(String columnName, String columnType, String dbType);
}
