package top.lingkang.finalsql.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author lingkang
 * Created by 2022/11/7
 */
public class GenerateUI extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("一键映射数据库表到实体类");
        FXMLLoader fxmlLoader = new FXMLLoader();
        VBox content = fxmlLoader.load(
                GenerateUI.class.getClassLoader().getResourceAsStream("generate-ui.fxml")
        );
        stage.setScene(new Scene(content));
        stage.show();
        stage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
