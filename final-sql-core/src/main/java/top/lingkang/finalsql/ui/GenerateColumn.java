package top.lingkang.finalsql.ui;

/**
 * @author lingkang
 * Created by 2022/11/8
 */
class GenerateColumn {
    private String name;
    private String columnName;
    private String type;
    private String importName;
    private String comment;// 注解、评论

    private boolean isPri;// 是否是主键

    public boolean isPri() {
        return isPri;
    }

    public void setPri(boolean pri) {
        isPri = pri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImportName() {
        return importName;
    }

    public void setImportName(String importName) {
        this.importName = importName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
