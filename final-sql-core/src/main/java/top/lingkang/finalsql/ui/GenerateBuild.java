package top.lingkang.finalsql.ui;

import top.lingkang.finalsql.sql.FinalSql;

import java.io.File;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/11/8
 */
public interface GenerateBuild {
    void build(FinalSql finalSql, GenerateProperties properties);

    default void outFile(String filePath, String content, boolean isCover) {
        File file = new File(filePath);
        if (file.exists()) {
            if (!isCover)
                return;
            // 检查内容是否一致，减少不必要重写
            List<String> o = GenerateUtils.fileReadByLine(file);
            List<String> list = GenerateUtils.stringReadByLine(content);
            if (o.size() == list.size()) {
                for (int i = 0; i < o.size(); i++) {
                    if (o.get(i).contains("* create by") && list.get(i).contains("* create by"))
                        continue;
                    if (!o.get(i).equals(list.get(i))) {
                        GenerateUtils.writeFile(file, content);
                        return;
                    }
                }
                return;// 此处表示已经不用重写了
            }else {
                GenerateUtils.writeFile(file, content);
            }
        } else {
            GenerateUtils.writeFile(file, content);
        }
    }
}
