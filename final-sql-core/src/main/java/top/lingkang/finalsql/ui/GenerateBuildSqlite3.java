package top.lingkang.finalsql.ui;

import top.lingkang.finalsql.sql.FinalSql;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author lingkang
 * Created by 2022/11/8
 */
public class GenerateBuildSqlite3 implements GenerateBuild {
    // 获取所有表
    private final String sql_tables = "SELECT name FROM sqlite_master WHERE type ='table';";
    private final String sql_column = "Pragma Table_Info(%s)";

    @Override
    public void build(FinalSql finalSql, GenerateProperties properties) {
        GenerateUtils.checkGenerateProperties(properties);
        List<String> tables = finalSql.selectForList(sql_tables, String.class);
        if (tables.isEmpty())
            return;

        String pageName = GenerateUtils.getPage(properties.getOutDir());
        String template = GenerateUtils.readFile(
                GenerateBuildSqlite3.class.getClassLoader().getResourceAsStream("java-template.txt")
        );
        if (!GenerateUtils.isBlank(pageName)) {
            pageName = "package " + pageName + ";";
        }
        template = template.replace("#package", pageName);
        String ignoreTablePrefix = properties.getIgnoreTablePrefix();

        HashSet<String> hasClassName = new HashSet<>();
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        for (String table : tables) {
            String className = GenerateUtils.handlerTableName(table, ignoreTablePrefix);
            if (hasClassName.contains(className)) {// 防止相同类型名冲突
                className = GenerateUtils.hasSomeName(className, hasClassName);
            }
            hasClassName.add(className);

            String t = template;
            t = t.replace("#date", date);
            t = t.replace("#table", table);
            t = t.replace("#className", className);

            HashSet<String> impl = new HashSet<>();
            // 列
            List<Map> columns = finalSql.selectForList(String.format(sql_column, table), Map.class);
            int keyNumber = 0;
            List<GenerateColumn> generateColumns = new ArrayList<>();
            for (Map<String, Object> map : columns) {
                String column_name = map.get("name").toString();
                String data_type = map.get("type").toString();
                String column_key = map.get("pk").toString();

                GenerateColumn column = GenerateUtils.columnTypeToJavaType(column_name, data_type, "mysql");
                if (!GenerateUtils.isBlank(column_key) && Integer.parseInt(column_key) == 1) {
                    keyNumber++;
                    column.setPri(true);
                }

                generateColumns.add(column);

                if (column.getImportName() != null) {
                    impl.add(column.getImportName());
                }
            }
            String imports = "";
            for (String im : impl) {
                imports = imports + im + "\n";
            }
            t = t.replace("#import", imports);

            String entity = GenerateUtils.columnToString(generateColumns, keyNumber);
            t = t.replace("#entity", entity);

            System.out.println(t);

            // out java file
            outFile(
                    properties.getOutDir() + File.separator + className + ".java",
                    t,
                    properties.isCover()
            );
        }
    }
}
