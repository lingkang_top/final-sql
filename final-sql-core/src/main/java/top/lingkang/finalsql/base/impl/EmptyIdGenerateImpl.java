package top.lingkang.finalsql.base.impl;

import top.lingkang.finalsql.base.IdGenerate;

/**
 * @author lingkang
 * Created by 2022/9/16
 */
public class EmptyIdGenerateImpl implements IdGenerate {
    @Override
    public String generate() {
        return null;
    }
}
