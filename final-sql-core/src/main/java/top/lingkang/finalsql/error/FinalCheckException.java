package top.lingkang.finalsql.error;

/**
 * @author lingkang
 * Created by 2022/9/8
 */
public class FinalCheckException extends FinalException{

    public FinalCheckException(String message) {
        super(message);
    }
}
