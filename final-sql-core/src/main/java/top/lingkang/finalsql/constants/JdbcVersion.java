package top.lingkang.finalsql.constants;

/**
 * @author lingkang
 * Created by 2022/9/16
 * jdbc协议版本
 */
public enum JdbcVersion {
    JDBC_V4, // 默认
    JDBC_V3
}
