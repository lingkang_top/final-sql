package top.lingkang.finalsql.dialect;

import top.lingkang.finalsql.sql.ExSqlEntity;

import java.util.ArrayList;

/**
 * @author lingkang
 * Created by 2022/12/3
 */
public class H2Dialect implements SqlDialect {
    @Override
    public String one(String sql) {
        return "select " + sql + " limit 1 offset 0";
    }

    @Override
    public String getTableName(String name) {
        return name;
    }

    @Override
    public String nextval(String column) {
        return "nextval('" + column + "')";
    }

    @Override
    public String rowSql(String sql, int start, int row) {
        return sql + " limit " + row + " offset " + start;
    }
}
