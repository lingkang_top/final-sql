package top.lingkang.finalsql.dialect;

import top.lingkang.finalsql.sql.ExSqlEntity;

import java.util.ArrayList;

/**
 * @author lingkang
 * Created by 2022/4/12
 * 此方言设配MySQL5.7版本，其他版本或许也适配
 */
public class Mysql57Dialect implements SqlDialect {
    @Override
    public String one(String sql) {
        return "select " + sql + " limit 1";
    }

    @Override
    public String count(String sql) {
        return "select count(*) " + sql;
    }

    @Override
    public String nextval(String column) {
        return null;
    }

    @Override
    public String rowSql(String sql, int start, int end) {
        return sql + " limit " + start + "," + end;
    }

}
