package top.lingkang.finalsql.sql;

/**
 * 扩展接口
 *
 * @author lingkang
 * created by 2023-04-03
 * @since 4.0.0
 */
public interface ExtendSql extends HumpSql {

    /**
     * 使用表映射实体类来编写好前部分更新，注意，sql语句执行时将被反驼峰
     *
     * @param entity 表映射实体类
     * @param sql    需要更新的SQL，例如："nickname='lingkang', createTime='2023-04-03'"
     *               ==> update {table} set nickname='lingkang', create_time='2023-04-03'
     * @return 更新个数
     */
    int updateTableSet(Class<?> entity, String sql);

    /**
     * 使用表映射实体类来编写好前部分更新，注意，sql语句执行时将被反驼峰
     *
     * @param entity 表映射实体类
     * @param sql    需要更新的SQL，例如："nickname='lingkang', createTime='2023-04-03'"
     *               ==> update {table} set nickname='lingkang', create_time='2023-04-03'
     * @param param  sql条件中?对应的参数，注意顺序
     * @return 更新个数
     */
    int updateTableSet(Class<?> entity, String sql, Object... param);

    /**
     * 使用表映射实体类来编写好前部分删除，注意，sql语句执行时将被反驼峰
     *
     * @param entity 表映射实体类
     * @param sql    需要删除的SQL条件，例如："nickname='lingkang' and createTime='2023-04-03'"
     *               ==> delete from {table} where nickname='lingkang' and create_time='2023-04-03'
     * @return 删除个数
     */
    int deleteTableWhere(Class<?> entity, String sql);

    /**
     * 使用表映射实体类来编写好前部分删除，注意，sql语句执行时将被反驼峰
     *
     * @param entity 表映射实体类
     * @param sql    需要删除的SQL条件，例如："nickname='lingkang' and createTime='2023-04-03'"
     *               ==> delete from {table} where nickname='lingkang' and create_time='2023-04-03'
     * @param param  sql条件中?对应的参数，注意顺序
     * @return 删除个数
     */
    int deleteTableWhere(Class<?> entity, String sql, Object... param);

}
