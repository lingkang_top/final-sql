package top.lingkang.finalsql.sql;

import top.lingkang.finalsql.error.FinalException;

import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * 2023/2/27
 * 驼峰sql，将驼峰命名自动转化字段为下划线执行
 * 例如 select userId from user -> select user_id from user
 * 需要注意，若sql的关键词也用大小写混合，将会被转换，可能会执行sql错误：
 * select userId fRom user -> select user_id f_rom user
 **/
public interface HumpSql {

    <T> List<T> humpSelectForList(String sql, Class<T> resultClass);

    <T> List<T> humpSelectForList(String sql, Class<T> resultClass, Object... param);

    <T> List<T> humpSelectForList(String sql, Class<T> resultClass, List<Object> param);

    Map<String, Object> humpSelectMap(String sql);

    Map<String, Object> humpSelectMap(String sql, Object... param);

    <T> T humpSelectForObject(String sql, Class<T> resultClass);

    <T> T humpSelectForObject(String sql, Class<T> resultClass, Object... param);

    <T> T humpSelectForObject(String sql, Class<T> resultClass, List<Object> param);

    /**
     * sql转化驼峰更新，注意,所有条件必须以 ? 作为入参
     *
     * @param sql update tableName set createTime=? where orderId=1 and uId in (?,?)
     *            ===> update table_name set create_time=? where order_id=1 and u_id in (?,?)
     * @return
     * @throws FinalException
     */
    int humpUpdate(String sql) throws FinalException;


    /**
     * sql转化驼峰更新，注意,所有条件必须以 ? 作为入参
     *
     * @param sql   update tableName set createTime=? where orderId=1 and uId in (?,?)
     *              ===> update table_name set create_time=? where order_id=1 and u_id in (?,?)
     * @param param
     * @return
     * @throws FinalException
     */
    int humpUpdate(String sql, Object... param) throws FinalException;

    /**
     * sql转化驼峰更新，注意,所有条件必须以 ? 作为入参
     *
     * @param sql    update tableName set createTime=? where orderId=1 and uId in (?,?)
     *               ===> update table_name set create_time=? where order_id=1 and u_id in (?,?)
     * @param params
     * @return
     * @throws FinalException
     */
    int humpUpdate(String sql, List params) throws FinalException;
}
