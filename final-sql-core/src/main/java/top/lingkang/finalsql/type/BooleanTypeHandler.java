package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class BooleanTypeHandler implements TypeHandler<Boolean> {
    @Override
    public Boolean getResult(ResultSet rs, String columnName) throws SQLException {
        boolean aBoolean = rs.getBoolean(columnName);
        return rs.wasNull() ? null : aBoolean;
    }

    @Override
    public Boolean getResult(ResultSet rs, int columnIndex) throws SQLException {
        boolean aBoolean = rs.getBoolean(columnIndex);
        return rs.wasNull() ? null : aBoolean;
    }
}
