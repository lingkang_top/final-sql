package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class ByteArrayTypeHandler implements TypeHandler<byte[]>{
    @Override
    public byte[] getResult(ResultSet rs, String columnName) throws SQLException {
        return rs.getBytes(columnName);
    }

    @Override
    public byte[] getResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getBytes(columnIndex);
    }
}
