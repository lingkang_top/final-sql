package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class DateTypeHandler implements TypeHandler<Date> {

    @Override
    public Date getResult(ResultSet rs, String columnName) throws SQLException {
        Timestamp sqlTimestamp = rs.getTimestamp(columnName);
        if (sqlTimestamp != null)
            return new Date(sqlTimestamp.getTime());
        return null;
    }

    @Override
    public Date getResult(ResultSet rs, int columnIndex) throws SQLException {
        Timestamp sqlTimestamp = rs.getTimestamp(columnIndex);
        if (sqlTimestamp != null)
            return new Date(sqlTimestamp.getTime());
        return null;
    }
}
