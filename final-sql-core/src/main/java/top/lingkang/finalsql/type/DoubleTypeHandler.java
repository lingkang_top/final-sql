package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class DoubleTypeHandler implements TypeHandler<Double> {
    @Override
    public Double getResult(ResultSet rs, String columnName) throws SQLException {
        double aDouble = rs.getDouble(columnName);
        return rs.wasNull()?null:aDouble;
    }

    @Override
    public Double getResult(ResultSet rs, int columnIndex) throws SQLException {
        double aDouble = rs.getDouble(columnIndex);
        return rs.wasNull()?null:aDouble;
    }
}
