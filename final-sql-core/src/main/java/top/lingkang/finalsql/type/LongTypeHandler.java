package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class LongTypeHandler implements TypeHandler<Long>{
    @Override
    public Long getResult(ResultSet rs, String columnName) throws SQLException {
        long aLong = rs.getLong(columnName);
        return rs.wasNull()?null:aLong;
    }

    @Override
    public Long getResult(ResultSet rs, int columnIndex) throws SQLException {
        long aLong = rs.getLong(columnIndex);
        return rs.wasNull()?null:aLong;
    }
}
