package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class ObjectTypeHandler implements TypeHandler<Object>{
    @Override
    public Object getResult(ResultSet rs, String columnName) throws SQLException {
        return rs.getObject(columnName);
    }

    @Override
    public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
        return  rs.getObject(columnIndex);
    }
}
