package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class IntegerTypeHandler implements TypeHandler<Integer> {

    @Override
    public Integer getResult(ResultSet rs, String columnName) throws SQLException {
        int anInt = rs.getInt(columnName);
        return rs.wasNull()?null:anInt;
    }

    @Override
    public Integer getResult(ResultSet rs, int columnIndex) throws SQLException {
        int anInt = rs.getInt(columnIndex);
        return rs.wasNull()?null:anInt;
    }
}
