package top.lingkang.finalsql.type;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class BigIntegerTypeHandler implements TypeHandler<BigInteger> {
    @Override
    public BigInteger getResult(ResultSet rs, String columnName) throws SQLException {
        BigDecimal bigDecimal = rs.getBigDecimal(columnName);
        if (bigDecimal == null)
            return null;
        return bigDecimal.toBigInteger();
    }

    @Override
    public BigInteger getResult(ResultSet rs, int columnIndex) throws SQLException {
        BigDecimal bigDecimal = rs.getBigDecimal(columnIndex);
        if (bigDecimal == null)
            return null;
        return bigDecimal.toBigInteger();
    }
}
