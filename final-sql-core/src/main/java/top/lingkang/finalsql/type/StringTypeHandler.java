package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class StringTypeHandler implements TypeHandler<String>{
    @Override
    public String getResult(ResultSet rs, String columnName) throws SQLException {
        return rs.getString(columnName);
    }

    @Override
    public String getResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getString(columnIndex);
    }
}
