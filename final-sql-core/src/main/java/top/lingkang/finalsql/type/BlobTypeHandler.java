package top.lingkang.finalsql.type;


import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class BlobTypeHandler implements TypeHandler<Blob> {
    @Override
    public Blob getResult(ResultSet rs, String columnName) throws SQLException {
        return rs.getBlob(columnName);
    }

    @Override
    public Blob getResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getBlob(columnIndex);
    }
}
