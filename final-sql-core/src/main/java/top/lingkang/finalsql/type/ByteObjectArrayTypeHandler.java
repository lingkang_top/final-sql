package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class ByteObjectArrayTypeHandler implements TypeHandler<Byte[]> {
    @Override
    public Byte[] getResult(ResultSet rs, String columnName) throws SQLException {
        byte[] bytes = rs.getBytes(columnName);
        if (rs.wasNull())
            return null;
        int length = bytes.length;
        Byte[] res = new Byte[length];
        for (int i = 0; i < length; i++) {
            res[i] = bytes[i];
        }
        return res;
    }

    @Override
    public Byte[] getResult(ResultSet rs, int columnIndex) throws SQLException {
        byte[] bytes = rs.getBytes(columnIndex);
        if (rs.wasNull())
            return null;
        int length = bytes.length;
        Byte[] res = new Byte[length];
        for (int i = 0; i < length; i++) {
            res[i] = bytes[i];
        }
        return res;
    }
}
