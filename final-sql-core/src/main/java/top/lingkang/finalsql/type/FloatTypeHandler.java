package top.lingkang.finalsql.type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class FloatTypeHandler implements TypeHandler<Float>{
    @Override
    public Float getResult(ResultSet rs, String columnName) throws SQLException {
        float aFloat = rs.getFloat(columnName);
        return rs.wasNull()?null:aFloat;
    }

    @Override
    public Float getResult(ResultSet rs, int columnIndex) throws SQLException {
        float aFloat = rs.getFloat(columnIndex);
        return rs.wasNull()?null:aFloat;
    }
}
