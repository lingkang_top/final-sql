package top.lingkang.finalsql.utils;

import top.lingkang.finalsql.annotation.Nullable;
import top.lingkang.finalsql.constants.IdType;
import top.lingkang.finalsql.type.TypeHandler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * @author lingkang
 * Created by 2022/9/8
 * 表对象缓存
 * 存储名称是因为 entity对象的属性名称不一定与数据库的列名称一一对应
 */
public class TabCache implements Serializable {
    private Class<?> clazz;
    private String tableName;
    private IdType idType;
    // id 字段专属
    private Field idColumnField;
    private String idColumnName;

    // columnName、columnField 均属于 @Column 表列
    private String[] columnName; // id 、 user_id 、role_id ...
    private Field[] columnField;

    // 字段、名称 不一定属于表的列，即不一定有 @Column 注解作用
    private String[] fieldName;// id 、 userId 、 roleId ...
    private Field[] fields;

    private TypeHandler[] typeHandler;
    // ---------------- get set ---------------


    public TypeHandler[] getTypeHandler() {
        return typeHandler;
    }

    public void setTypeHandler(TypeHandler[] typeHandler) {
        this.typeHandler = typeHandler;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Nullable
    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    @Nullable
    public Field getIdColumnField() {
        return idColumnField;
    }

    public void setIdColumnField(Field idColumnField) {
        this.idColumnField = idColumnField;
    }

    @Nullable
    public String getIdColumnName() {
        return idColumnName;
    }

    public void setIdColumnName(String idColumnName) {
        this.idColumnName = idColumnName;
    }

    public String[] getColumnName() {
        return columnName;
    }

    public void setColumnName(String[] columnName) {
        this.columnName = columnName;
    }

    public Field[] getColumnField() {
        return columnField;
    }

    public void setColumnField(Field[] columnField) {
        this.columnField = columnField;
    }

    public String[] getFieldName() {
        return fieldName;
    }

    public void setFieldName(String[] fieldName) {
        this.fieldName = fieldName;
    }

    public Field[] getFields() {
        return fields;
    }

    public void setFields(Field[] fields) {
        this.fields = fields;
    }
}
