package mysql;

import mysql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import sqlite.ui.entity.User;

import java.util.Date;

/**
 * @author lingkang
 * created by 2023/4/3
 */
public class ExtendSqlTest extends BaseMysqlTest {
    @Test
    public void update(){
        int set = finalSql.updateTableSet(
                User.class,
                "createTime=? where username=?",
                new Date(),"lingkang1"
                );
        System.out.printf("success number: %d%n",set);
    }


}
