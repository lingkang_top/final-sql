package mysql;

import mysql.entity.UserAuto;
import mysql.entity.UserNotId;
import mysql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.sql.Condition;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class SelectOneMysqlTest extends BaseMysqlTest {
    @Test
    public void selectOne() {
        finalSql.selectOne(UserAuto.class);

        // 查询一个结果：查询 type=2 并且进行倒序
        finalSql.selectOne(UserNotId.class, new Condition().lt("id", 530378).orderByDesc("id"));
    }

    @Test
    public void oneWhere() {
        finalSql.selectOne(UserNotId.class, new Condition().eq("id", "1"));
        finalSql.selectOne(UserAuto.class, new Condition().eq("id", "530369"));
    }

    @Test
    public void one() {
        finalSql.selectOne(UserAuto.class, new Condition().eq("createTime", "132"));
        finalSql.selectOne(UserAuto.class, new Condition().like("createTime", "13"));
        finalSql.selectOne(UserNotId.class, new Condition().leftLike("create_time", "0"));
        finalSql.selectOne(UserNotId.class, new Condition().rightLike("id", "5682"));
    }
}
