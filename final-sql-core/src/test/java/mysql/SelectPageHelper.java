package mysql;

import mysql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.dialect.Mysql57Dialect;
import top.lingkang.finalsql.sql.ExSqlEntity;

import java.util.HashMap;
import java.util.List;

/**
 * @author lingkang
 * 2023/2/2
 **/

public class SelectPageHelper extends BaseMysqlTest {
    @Test
    public void page1() {
        finalSql.startPage(1, 10);
        List<HashMap> hashMaps = finalSql.selectForList("SELECT\n" +
                "\t*,\n" +
                "\t(\n" +
                "\tSELECT\n" +
                "\t\tGROUP_CONCAT( username ) \n" +
                "\tFROM\n" +
                "\tUSER \n" +
                "\tWHERE\n" +
                "\t\tUSER.username = username\n" +
                "\t) AS usernames \n" +
                "FROM\n" +
                "USER", HashMap.class);
        System.out.println(hashMaps);
        System.out.println(finalSql.getPageInfo());
    }


    @Test
    public void page2() {
        finalSql.startPage(1, 10);
        List<HashMap> hashMaps = finalSql.selectForList(
                "select * from user u left JOIN f_file f on f.`name`=u.username" +
                        " where f.id is not NULL",
                HashMap.class);
        System.out.println(hashMaps);
        System.out.println(finalSql.getPageInfo());
    }
}
