package mysql;

import lombok.extern.slf4j.Slf4j;
import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
@Slf4j
public class SelectForObject extends BaseMysqlTest {
    @Test
    public void obj() {
        finalSql.selectForObject("select * from user", UserAuto.class);

        finalSql.selectForObject("select * from user where id<?", UserAuto.class, 530374);

        List<Object> params = new ArrayList<>();
        params.add(530374);
        finalSql.selectForObject("select count(*) from user where id<?", Integer.class, params);
    }

    @Test
    public void page() {
        log.info("分页查询");
        finalSql.startPage(1, 5);
        List<UserAuto> select = finalSql.select(UserAuto.class);
        System.out.println(finalSql.getPageInfo());
    }

    @Test
    public void other() {
        BigDecimal bigDecimal = finalSql.humpSelectForObject("select bigDecimal from other", BigDecimal.class);
        System.out.println(bigDecimal);
    }

    @Test
    public void test01() {
    }
}
