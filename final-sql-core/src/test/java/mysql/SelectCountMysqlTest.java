package mysql;

import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import mysql.entity.UserInput;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.sql.Condition;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class SelectCountMysqlTest extends BaseMysqlTest {
    @Test
    public void count() {
        finalSql.selectCount(UserAuto.class);

        finalSql.selectCount(UserInput.class, new Condition().eq("id", 530373));

        finalSql.selectCount(UserInput.class, new Condition().like("password", "pwd"));
    }

    @Test
    public void test() {
        // in 查询个数
        List<Integer> in = new ArrayList<>();
        in.add(1);
        in.add(5);
        finalSql.selectCount(UserInput.class, new Condition().andIn("id", in));

    }
}
