package mysql;

import lombok.extern.slf4j.Slf4j;
import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import mysql.entity.UserIdNotColumn;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.sql.Condition;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
@Slf4j
public class UpdateMysqlTest extends BaseMysqlTest {
    @Test
    public void update() {
        UserAuto user = finalSql.selectOne(UserAuto.class, new Condition().eq("id", 530380));
        user.setCreateTime(new Date());
        finalSql.update(user);
        System.out.println(user);
    }

    @Test
    public void update2() {
        UserIdNotColumn user = new UserIdNotColumn();
        user.setId(530370);
        user.setCreateTime(new Date());
        finalSql.update(user);
    }

    @Test
    public void where() {
        UserAuto userAuto = finalSql.selectOne(UserAuto.class);
        int num = finalSql.update(userAuto, new Condition().eq("num", 0));
        System.out.println(num);
    }

    @Test
    public void nativeHumpUpdate() {
        UserAuto user = finalSql.selectOne(UserAuto.class);
        finalSql.humpUpdate(
                "update user set id=? , createTime=? where id=?",
                user.getId(), new Date(), user.getId()
        );
    }

}
