package mysql.file;

import mysql.common.BaseMysqlTest;
import mysql.entity.FileByte;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.sql.Condition;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class FileByteMysqlTest extends BaseMysqlTest {
    @Test
    public void insert() throws Exception {
        File file = new File(FileBlobMysqlTest.class.getClassLoader().getResource("init-mysql.sql").getFile());
        FileByte fileByte = new FileByte();
        fileByte.setId((int)System.currentTimeMillis());
        fileByte.setName("file-byte");
        byte[] bytes = new byte[(int) file.length()];
        new FileInputStream(file).read(bytes);
        fileByte.setContent(bytes);
        finalSql.insert(fileByte);
    }

    @Test
    public void read() throws Exception {
        File file = new File("d://final-sql-temp-file-byte.sql");
        if (file.exists())
            file.delete();

        FileByte fileByte = finalSql.selectOne(FileByte.class, new Condition().eq("name", "file-byte"));
        FileOutputStream out = new FileOutputStream(file);
        out.write(fileByte.getContent());
        out.flush();
        out.close();
        System.out.println("success!");
    }
}
