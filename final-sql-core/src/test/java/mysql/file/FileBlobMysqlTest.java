package mysql.file;

import com.mysql.cj.jdbc.Blob;
import mysql.common.BaseMysqlTest;
import mysql.entity.FileBlob;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
public class FileBlobMysqlTest extends BaseMysqlTest {
    @Test
    public void insert()throws Exception{
        File file=new File(FileBlobMysqlTest.class.getClassLoader().getResource("init-mysql.sql").getFile());
        FileBlob fileBlob =new FileBlob();
        fileBlob.setId((int)System.currentTimeMillis());
        fileBlob.setName("123");
        byte[] bytes=new byte[(int) file.length()];
        new FileInputStream(file).read(bytes);
        Blob blob=new Blob(bytes,null);
        fileBlob.setContent(blob);
        finalSql.insert(fileBlob);
    }

    @Test
    public void select()throws Exception{
        FileBlob fileBlob = finalSql.selectOne(FileBlob.class);
        java.sql.Blob content = fileBlob.getContent();

        File file=new File("d://final-sql-temp.sql");
        if (file.exists())
            file.delete();

        FileOutputStream out = new FileOutputStream(file);
        byte[] by=new byte[(int) content.length()];
        content.getBinaryStream().read(by);
        out.write(by);
        out.flush();

        out.close();
        content.getBinaryStream().close();
    }
}
