package mysql;

import mysql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.sql.ResultCallback;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class NativeSQL extends BaseMysqlTest {
    @Test
    public void select(){
        List<Object> objects = finalSql.nativeSelect("select * from user", new ResultCallback<Object>() {
            @Override
            public Object callback(ResultSet result) throws Exception {
                return result.getObject(1);
            }
        });
        System.out.println(objects);
    }

    @Test
    public void update(){
        finalSql.nativeUpdate("update user set create_time=? where id=530368",new Date());
    }

}
