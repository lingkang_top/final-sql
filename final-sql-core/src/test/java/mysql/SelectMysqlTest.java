package mysql;

import cn.hutool.core.lang.Assert;
import mysql.entity.UserAuto;
import mysql.entity.UserIdNotColumn;
import mysql.entity.UserInput;
import mysql.entity.UserNotId;
import mysql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.lingkang.finalsql.sql.Condition;

import java.util.List;


/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class SelectMysqlTest extends BaseMysqlTest {
    Logger log = LoggerFactory.getLogger(SelectMysqlTest.class);

    @Test
    public void select() {
        log.info("基本测试");
        Assert.notNull(finalSql.select(UserAuto.class));
        Assert.notNull(finalSql.select(UserInput.class, new Condition()));
    }

    @Test
    public void where(){
        finalSql.select(UserInput.class, new Condition().eq("id", "123"));
        finalSql.select(UserInput.class, new Condition().or("id", "123").eq("id", "530369"));
    }

    @Test
    public void like(){
        finalSql.select(UserNotId.class, new Condition().like("id", "%1%"));
        finalSql.select(UserNotId.class, new Condition().like("create_time", "13"));
        finalSql.select(UserNotId.class, new Condition().leftLike("create_time", "0"));
        finalSql.select(UserNotId.class, new Condition().leftLike("username", "lingkang"));

    }

    @Test
    public void unHump(){
        finalSql.select(UserNotId.class, new Condition().like("createTime", "21"));
    }

    @Test
    public void idNotAddColumn() {
        log.info("增加@Id，不添加@Column测试");
        finalSql.select(UserIdNotColumn.class,new Condition().eq("id","530369"));
    }

    @Test
    public void test02(){
        // 查询对象列表， 约定大于配置， create_time 在返回结果时将被转化为 createTime 驼峰命名
        List<UserAuto> userAutos = finalSql.selectForList("select id,username,create_time from user", UserAuto.class);
        System.out.println(userAutos);

    }
}
