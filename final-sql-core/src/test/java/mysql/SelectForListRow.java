package mysql;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import mysql.entity.UserIdNotColumn;
import mysql.entity.UserNotId;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class SelectForListRow extends BaseMysqlTest {

    @Test
    public void list() {
        finalSql.selectForListRow("select * from user where id=530376", UserAuto.class, 1);

        finalSql.selectForListRow("select * from user", UserIdNotColumn.class, 100);

        finalSql.selectForListRow("select * from user", UserNotId.class, 1000);
    }

    @Test
    public void where() {


        Assert.isTrue(CollUtil.isEmpty(finalSql.selectForListRow("select * from user where id=?", UserAuto.class, 10, "1")));
        List<Object> params = new ArrayList<>();
        params.add("1");
        Assert.isTrue(CollUtil.isEmpty(finalSql.selectForListRow("select * from user where id=?", UserAuto.class, 1, params)));
    }
}
