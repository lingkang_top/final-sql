package mysql;

import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import mysql.entity.UserObjectType;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.utils.NameUtils;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/10/8
 */
public class Other extends BaseMysqlTest {
    @Test
    public void test01() throws Exception {
        DataSource dataSource = finalSql.getDataSource();
        System.out.println(dataSource);
    }

    @Test
    public void object() {
        System.out.println(finalSql.selectOne(UserObjectType.class));
        System.out.println(finalSql.selectOne(UserAuto.class));
    }

    @Test
    public void HumpUpdate() {
        System.out.println(NameUtils.unHump("update c_user set createTime=? where orderId=1 and uId in (?,?)"));
    }
}
