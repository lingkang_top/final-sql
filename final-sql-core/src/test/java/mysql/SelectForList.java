package mysql;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import mysql.entity.UserIdNotColumn;
import mysql.entity.UserNotId;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class SelectForList extends BaseMysqlTest {
    @Test
    public void list() {
        finalSql.selectForList("select * from user", UserAuto.class);

        finalSql.selectForList("select * from user", UserIdNotColumn.class);

        finalSql.selectForList("select * from user", UserNotId.class);
    }

    @Test
    public void where() {
        Assert.isTrue(CollUtil.isEmpty(finalSql.selectForList("select * from user where id=?", UserAuto.class, "1")));
        List<Object> params = new ArrayList<>();
        params.add("1");
        Assert.isTrue(CollUtil.isEmpty(finalSql.selectForList("select * from user where id=?", UserAuto.class, params)));
    }

    @Test
    public void map(){
        finalSql.selectForList("select * from user", Map.class);
    }

    @Test
    public void bean(){
        finalSql.selectForList("select * from user", UserAuto.class);
    }
}
