package mysql;

import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * @author lingkang
 * Created by 2022/11/12
 */
public class Select extends BaseMysqlTest {
    @Test
    public void select(){
        List<UserAuto> select = finalSql.select(UserAuto.class);
        assert select.get(0).getCreateTime()!=null;
        System.out.println(select);
    }

    @Test
    public void one(){

    }
}
