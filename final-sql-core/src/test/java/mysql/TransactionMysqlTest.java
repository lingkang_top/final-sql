package mysql;

import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.sql.Condition;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class TransactionMysqlTest extends BaseMysqlTest {
    @Test
    public void insert() {
        finalSql.beginTransaction();
        UserAuto userAuto=new UserAuto();
        userAuto.setUsername("123");
        userAuto.setCreateTime(new Date());
        finalSql.insert(userAuto);
        finalSql.rollbackTransaction();// 回滚
    }

    @Test
    public void update(){
        finalSql.beginTransaction();
        UserAuto userAuto = finalSql.selectOne(UserAuto.class, new Condition().eq("id", "530369"));
        userAuto.setUsername("transaction-");
        userAuto.setCreateTime(new Date());
        finalSql.update(userAuto);
        finalSql.commitTransaction();
    }
}
