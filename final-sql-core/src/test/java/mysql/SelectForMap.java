package mysql;

import mysql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class SelectForMap extends BaseMysqlTest {
    @Test
    public void test() {
        finalSql.selectForMap("select * from user");

        finalSql.selectForMap("select * from user where id<?", 530374);

        List<Object> params = new ArrayList<>();
        params.add(530374);
        finalSql.selectForMap("select count(*) as count from user where id<?", params);
    }
}
