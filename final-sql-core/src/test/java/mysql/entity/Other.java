package mysql.entity;

import lombok.Data;
import top.lingkang.finalsql.annotation.Column;
import top.lingkang.finalsql.annotation.Table;

import java.math.BigDecimal;

/**
 * @author lingkang
 * Created by 2023/7/8
 */
@Data
@Table("user")
public class Other {
    @Column
    private BigDecimal bigDecimal;

    // 其他无关字段
    private String status;
}
