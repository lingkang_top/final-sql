package mysql.entity;

import lombok.Data;
import top.lingkang.finalsql.annotation.Column;
import top.lingkang.finalsql.annotation.Id;
import top.lingkang.finalsql.annotation.Table;
import top.lingkang.finalsql.constants.IdType;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
@Data
@Table("user")
public class UserObjectType {
    @Id(value = IdType.INPUT)
    @Column
    private Integer id;
    @Column
    private Object num;
    @Column
    private String username;
    @Column
    private Object password;
    @Column
    private Object createTime;

    // 其他无关字段
    private String status;
}
