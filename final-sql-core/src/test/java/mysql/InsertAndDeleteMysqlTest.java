package mysql;

import cn.hutool.core.lang.Assert;
import mysql.common.BaseMysqlTest;
import mysql.entity.UserAuto;
import mysql.entity.UserIdNotColumn;
import mysql.entity.UserInput;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.lingkang.finalsql.sql.Condition;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class InsertAndDeleteMysqlTest extends BaseMysqlTest {
    private static final Logger log = LoggerFactory.getLogger(InsertAndDeleteMysqlTest.class);

    @Test
    public void auto() {
        // 插入
        UserAuto user = new UserAuto();
        user.setCreateTime(new Date());
        user.setUsername("lingkang");
        finalSql.insert(user);
        Assert.notNull(user.getId());

        // 删除
        Assert.notNull(finalSql.delete(user));

        user.setId(19951219);
        finalSql.insert(user);
        Assert.isTrue(user.getId() == 19951219);

        Assert.notNull(finalSql.delete(user));

        UserIdNotColumn userIdNotColumn=new UserIdNotColumn();
        userIdNotColumn.setId(530372);
        userIdNotColumn.setUsername("not asd");
        Assert.isTrue(finalSql.delete(userIdNotColumn)==0);
    }

    @Test
    public void input() {
        // 插入
        UserInput user = new UserInput();
        user.setCreateTime(new Date());
        user.setUsername("lingkang");
        user.setId(125);
        finalSql.insert(user);
        Assert.notNull(user.getId());

        // 删除
        Assert.notNull(finalSql.delete(user));
        boolean ok = false;
        try {
            user.setId(null);
            finalSql.insert(user);
            Assert.notNull(finalSql.delete(user));
            ok = true;
        } catch (Exception e) {
            ok = false;
            Assert.notNull(e);
        }
        Assert.isFalse(ok, "IdType.INPUT 类型不正常");
    }

    @Test
    public void idNotColumn() {
        // 插入
        UserIdNotColumn user = new UserIdNotColumn();
        user.setCreateTime(new Date());
        user.setUsername("lingkang");
        finalSql.insert(user);
        Assert.notNull(user.getId());

        // 删除
        Assert.notNull(finalSql.delete(user));
        user.setId(1995);
        finalSql.insert(user);
        Assert.notNull(finalSql.delete(user));
    }

    @Test
    public void where() {
        UserAuto user = new UserAuto();
        user.setCreateTime(new Date());
        user.setUsername("insert-where");
        finalSql.insert(user);
        Assert.notNull(user.getId());

        user.setId(4399123);
        Assert.isTrue(finalSql.delete(user) == 0);

        user.setId(null);
        Assert.isTrue(finalSql.delete(user) == 1);

        user = new UserAuto();
        user.setCreateTime(new Date());
        user.setUsername("insert-where123");
        finalSql.insert(user);
        Assert.notNull(user.getId());

        UserAuto user1 = new UserAuto();
        user1.setUsername("insert-where123-not");
        Assert.isTrue(finalSql.delete(user1) == 0);
        Assert.isTrue(finalSql.delete(new UserAuto(), new Condition().eq("id", user.getId())) == 1);
    }


    @Test
    public void batch() {
        List<UserAuto> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            UserAuto user = new UserAuto();
            user.setNum(1219 + i);
            user.setCreateTime(new Date());
            list.add(user);
        }

        Assert.isTrue(finalSql.batchInsert(list) == list.size());

        List<Integer> ids = new ArrayList<>();
        for (UserAuto user : list) {
            ids.add(user.getId());
        }

        Assert.isTrue(finalSql.deleteByIds(UserAuto.class, ids) == ids.size());
    }

    @Test
    public void batch2() {
        List<UserAuto> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            UserAuto user = new UserAuto();
            user.setNum(1219 + i);
            user.setCreateTime(new Date());
            list.add(user);
        }

        Assert.isTrue(finalSql.batchInsert(list) == list.size());

        List<Integer> ids = new ArrayList<>();
        for (UserAuto user : list) {
            ids.add(user.getId());
        }

        Assert.isTrue(finalSql.deleteByIds(UserAuto.class, ids.toArray(new Integer[0])) == ids.size());
    }
}
