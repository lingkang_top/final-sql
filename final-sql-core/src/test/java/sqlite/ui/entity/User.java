package sqlite.ui.entity;

import lombok.Data;
import java.util.Date;

import top.lingkang.finalsql.annotation.Column;
import top.lingkang.finalsql.annotation.Id;
import top.lingkang.finalsql.annotation.Table;

/**
 * create by 2022-12-02
**/
@Data
@Table("user")
public class User {
    @Id
    private Integer id;

    @Column
    private Object num;

    @Column
    private String username;

    @Column
    private String password;

    @Column("create_time")
    private Date createTime;


}
