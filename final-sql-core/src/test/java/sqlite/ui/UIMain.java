package sqlite.ui;

import top.lingkang.finalsql.ui.GenerateUI;

/**
 * @author lingkang
 * Created by 2022/12/2
 */
public class UIMain {
    public static void main(String[] args) {
        GenerateUI.main(args);
    }
}
