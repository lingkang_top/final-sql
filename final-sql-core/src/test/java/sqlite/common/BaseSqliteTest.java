package sqlite.common;

import org.sqlite.SQLiteConnection;
import org.sqlite.jdbc4.JDBC4Connection;
import top.lingkang.finalsql.config.SqlConfig;
import top.lingkang.finalsql.dev.FinalSqlDevDataSource;
import top.lingkang.finalsql.sql.FinalSql;
import top.lingkang.finalsql.sql.core.FinalSqlManage;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
public class BaseSqliteTest {
    protected static FinalSqlDevDataSource dataSource;
    protected static FinalSql finalSql;
    static {
        try {
            init();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void init() throws Exception {
        /*HikariDataSource ds = new HikariDataSource();
        ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
        ds.setJdbcUrl("jdbc:mysql://localhost:3306/test?serverTimezone=UTC");
        ds.setUsername("root");
        ds.setPassword("123456");
*/
        if (!new File("d:\\final-sql.db").exists()){
            Connection conn = DriverManager.getConnection("jdbc:sqlite:d:/final-sql.db", "root", "123456");
            System.out.println(conn.isClosed());
        }
        dataSource=new FinalSqlDevDataSource(
                "org.sqlite.JDBC",
                "jdbc:sqlite:d:/final-sql.db",
                "root",
                "123456"
        );

        SqlConfig sqlConfig = new SqlConfig(dataSource);
        sqlConfig.setShowLog(true);
        // sqlConfig.setUsePageHelper(false);
        finalSql = new FinalSqlManage(sqlConfig);
    }
}
