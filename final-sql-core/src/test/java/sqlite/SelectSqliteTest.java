package sqlite;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import sqlite.common.BaseSqliteTest;
import sqlite.entity.UserAuto;

import java.util.List;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
@Slf4j
public class SelectSqliteTest extends BaseSqliteTest {
    @Test
    public void select() {
        List<UserAuto> select = finalSql.select(UserAuto.class);
        System.out.println(select);
    }

    @Test
    public void page(){
        log.info("分页查询");
        finalSql.startPage(1,5);
        List<mysql.entity.UserAuto> select = finalSql.select(mysql.entity.UserAuto.class);
        System.out.println(finalSql.getPageInfo());
    }
}
