package other;

import top.lingkang.finalsql.utils.NameUtils;

/**
 * @author lingkang
 * 2023/2/16
 **/
public class Demo02 {
    public static void main(String[] args) {
        char a='a';
        a= (char) (a-32);
        System.out.println(a);
        char b='_';
        System.out.println((int)b);

        System.out.println(NameUtils.toHump("u_user"));
        System.out.println(NameUtils.toHump("f_user_role"));
        System.out.println(NameUtils.toHump("username"));

        System.out.println(NameUtils.unHump("UUser"));

        System.out.println(NameUtils.unHump("username"));
        System.out.println(NameUtils.unHump("fUserRole"));

        System.out.println("----");
        System.out.println(NameUtils.unHump("pId"));
        System.out.println(NameUtils.toHump("pId"));
        System.out.println("----");
        System.out.println(NameUtils.toHump("f_store_session"));
    }
}
