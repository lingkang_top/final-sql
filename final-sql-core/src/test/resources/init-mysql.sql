-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.7.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `f_file`
--

DROP TABLE IF EXISTS `f_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `f_file` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) DEFAULT NULL,
                          `content` longblob,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f_file`
--

LOCK TABLES `f_file` WRITE;
/*!40000 ALTER TABLE `f_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `f_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `num` int(11) DEFAULT NULL,
                        `username` varchar(255) DEFAULT NULL COMMENT '用户名',
                        `password` varchar(255) DEFAULT NULL,
                        `create_time` datetime DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=568278 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (530368,0,'4399','pwd0','2022-04-16 18:14:24',NULL),(530369,1,'lingkang1','pwd1','2022-04-16 18:14:24',NULL),(530370,2,'lingkang2','pwd2','2022-04-16 18:14:24',NULL),(530371,3,'lingkang3','pwd3','2022-04-16 18:14:24',NULL),(530372,4,'lingkang4','pwd4','2022-04-16 18:14:24',NULL),(530373,5,'lingkang5','pwd5','2022-04-16 18:14:24',NULL),(530374,6,'lingkang6','pwd6','2022-04-16 18:14:24',NULL),(530375,7,'lingkang7','pwd7','2022-04-16 18:14:24',NULL),(530376,8,'lingkang8','pwd8','2022-04-16 18:14:24',NULL),(530377,9,'lingkang9','pwd9','2022-04-16 18:14:24',NULL),(530378,10,'lingkang10','pwd10','2022-04-16 18:14:24',NULL),(530379,11,'lingkang11','pwd11','2022-04-16 18:14:24',NULL),(530380,12,'lingkang12','pwd12','2022-04-16 18:14:24',NULL),(530381,13,'lingkang13','pwd13','2022-04-16 18:14:24',NULL),(568275,NULL,'lingkang0',NULL,'2022-04-18 00:00:00',NULL),(568276,NULL,'lingkang','123456','2022-05-03 13:00:29',NULL),(568277,NULL,'lingkang123','123456123123','2022-05-03 21:21:20',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-08 15:04:04

    --
-- Table structure for table `other`
--

DROP TABLE IF EXISTS `other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `other` (
    `big_decimal` decimal(8,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other`
--

LOCK TABLES `other` WRITE;
/*!40000 ALTER TABLE `other` DISABLE KEYS */;
INSERT INTO `other` VALUES (0.10),(0.50),(55.88),(4.11);
/*!40000 ALTER TABLE `other` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;