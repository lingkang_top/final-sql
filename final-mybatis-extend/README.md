# final-mybatis-extend

#### 介绍

final-mybatis-extend 对mybatis拓展，让final-sql-core支持一定的mybatis的xml模板引擎能力，从而现实复杂SQL的编写
<br>

### 快速开始

#### 1、基于 Final-sql

```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-sql-core</artifactId>
    <version>x.y.z</version>
</dependency>
<dependency>
    <artifactId>final-sql</artifactId>
    <artifactId>final-mybatis-extend</artifactId>
    <version>x.y.z</version>
</dependency>
```
`x.y.z` 请选择最新的发行版本，maven公共仓库：[https://mvnrepository.com/artifact/top.lingkang/final-sql](https://mvnrepository.com/artifact/top.lingkang/final-sql)

编写 resources/mapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="mapper">
    <select id="selectUser">
        select *
        from user where 1 = 1
        <if test="id">
            and id = #{id}
        </if>
    </select>
</mapper>
```

调用
```java
@Test
public void test99() {
    FinalExtendConfig finalExtendConfig = new FinalExtendConfig();
    finalExtendConfig.setShowSql(true);// 日志打印SQL
    // 加载 xml
    finalExtendConfig.addXml(
    //  getClass().getClassLoader().getResourceAsStream("finalsqlMapper.xml"),
        getClass().getClassLoader().getResourceAsStream("testMapper.xml")
    );

    // 一定要应用上 finalSql 否则空指针
    MybatisTemplate mybatisTemplate = new FinalMybatisTemplate(finalExtendConfig).apply(finalSql);

    List<UserAuto> selectUser = mybatisTemplate.select("selectUser", UserAuto.class);
    System.out.println(selectUser);

    selectUser = mybatisTemplate.select("selectUser", UserAuto.class, new ParamObject().add("id", 530375));
    System.out.println(selectUser);
}
```

`上面的 mybatisTemplate 为核心操作类，可注册到spring组件中`

```java
    @Bean
    public MybatisTemplate mybatisTemplate(@Qualifier("finalSql") FinalSql finalSql) {
        // 初始化mybatis扩展
        FinalExtendConfig finalExtendConfig = new FinalExtendConfig();
        // finalExtendConfig.setShowSql(true);// 日志打印SQL
        // 加载 xml
        finalExtendConfig.addXml(
//                getClass().getClassLoader().getResourceAsStream("mapper/goodsMapper.xml"),
//                getClass().getClassLoader().getResourceAsStream("mapper/userMapper.xml"),
//                getClass().getClassLoader().getResourceAsStream("mapper/commonMapper.xml")
        );
        return new FinalMybatisTemplate(finalExtendConfig).apply(finalSql);
    }
```

调用
```java
    @Autowired
    private MybatisTemplate mybatisTemplate;
```


### 其他
```java
List<UserAuto> selectUser = mybatisTemplate.select("select01", UserAuto.class);

        int update01 = mybatisTemplate.update("update01", new ParamObject().add("id", 530380));

        int insert01 = mybatisTemplate.insert("insert01");
        System.out.println(insert01);

        int delete01 = mybatisTemplate.delete("delete01", new ParamObject().add("id", 99999));
        System.out.println(delete01);

```