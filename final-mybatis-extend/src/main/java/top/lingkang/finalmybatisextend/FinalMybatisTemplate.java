package top.lingkang.finalmybatisextend;

import top.lingkang.finalmybatisextend.base.ParamObject;
import top.lingkang.finalmybatisextend.entity.SqlResult;
import top.lingkang.finalmybatisextend.utils.AssertUtils;
import top.lingkang.finalsql.sql.FinalSql;

import java.util.List;

/**
 * @author lingkang
 * Created by 2022/10/10
 */
public class FinalMybatisTemplate extends AbstractBaseExtend implements MybatisTemplate {

    public FinalMybatisTemplate(FinalExtendConfig extendConfig) {
        super(extendConfig);
    }

    private FinalSql finalSql;

    public FinalMybatisTemplate apply(FinalSql finalSql) {
        this.finalSql = finalSql;
        return this;
    }

    // ---------------------------- start impl

    @Override
    public <T> List<T> select(String id, Class<T> resultClass) {
        return select(id, resultClass, null);
    }

    @Override
    public <T> List<T> select(String id, Class<T> resultClass, ParamObject paramObject) {
        AssertUtils.notNull(resultClass, "查询结果类不能为空：resultClass=" + resultClass);
        SqlResult sqlResult = get(id, paramObject);
        return finalSql.selectForList(sqlResult.getSql(), resultClass, sqlResult.getParams());
    }

    @Override
    public <T> T selectOne(String id, Class<T> resultClass) {
        return selectOne(id, resultClass, null);
    }

    @Override
    public <T> T selectOne(String id, Class<T> resultClass, ParamObject paramObject) {
        AssertUtils.notNull(resultClass, "查询结果类不能为空：resultClass=" + resultClass);
        SqlResult sqlResult = get(id, paramObject);
        List<T> ts = finalSql.selectForListRow(sqlResult.getSql(), resultClass, 1, sqlResult.getParams());
        if (ts.isEmpty())
            return null;
        return ts.get(0);
    }

    @Override
    public int update(String id) {
        return update(id, null);
    }

    @Override
    public int update(String id, ParamObject paramObject) {
        SqlResult sqlResult = get(id, paramObject);
        return finalSql.nativeUpdate(sqlResult.getSql(), sqlResult.getParams());
    }

    @Override
    public int insert(String id) {
        return insert(id, null);
    }

    @Override
    public int insert(String id, ParamObject paramObject) {
        SqlResult sqlResult = get(id, paramObject);
        return finalSql.nativeUpdate(sqlResult.getSql(), sqlResult.getParams());
    }

    @Override
    public <T> T insertReturnGeneratedKey(String id, Class<T> resultClass) {
        return insertReturnGeneratedKey(id, resultClass, null);
    }

    @Override
    public <T> T insertReturnGeneratedKey(String id, Class<T> resultClass, ParamObject paramObject) {
        SqlResult sqlResult = get(id, paramObject);
        return finalSql.insertReturnGeneratedKey(sqlResult.getSql(), resultClass, sqlResult.getParams());
    }

    @Override
    public int delete(String id) {
        return delete(id, null);
    }

    @Override
    public int delete(String id, ParamObject paramObject) {
        SqlResult sqlResult = get(id, paramObject);
        return finalSql.nativeUpdate(sqlResult.getSql(), sqlResult.getParams());
    }

    @Override
    public SqlResult get(String id) {
        return super.get(id, null);
    }

    @Override
    public SqlResult get(String id, ParamObject paramObject) {
        return super.get(id, paramObject);
    }

    @Override
    public <T> List<T> selectSqlResult(SqlResult sqlResult, Class<T> resultClass) {
        return finalSql.selectForList(sqlResult.getSql(), resultClass, sqlResult.getParams());
    }

    @Override
    public int updateSqlResult(SqlResult sqlResult) {
        return finalSql.humpUpdate(sqlResult.getSql(), sqlResult.getParams());
    }
}
