package top.lingkang.finalmybatisextend.entity;

import java.util.List;

/**
 * @author lingkang
 * Created by 2022/10/10
 * 拓展结果
 */
public class SqlResult {
    private String sql;
    private List<Object> params;

    public SqlResult() {
    }

    public SqlResult(String sql, List<Object> params) {
        this.sql = sql;
        this.params = params;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<Object> getParams() {
        return params;
    }

    public void setParams(List<Object> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "ExtendObject{" +
                "sql='" + sql + '\'' +
                ", params=" + params +
                '}';
    }
}
