package top.lingkang.finalmybatisextend;

import top.lingkang.finalmybatisextend.base.ParamObject;
import top.lingkang.finalmybatisextend.entity.SqlResult;

import java.util.List;

/**
 * @author lingkang
 * Created by 2022/10/10
 * @since 3.0.0
 */
public interface MybatisTemplate {

    <T> List<T> select(String id, Class<T> resultClass);

    <T> List<T> select(String id, Class<T> resultClass, ParamObject paramObject);

    <T> T selectOne(String id, Class<T> resultClass);

    <T> T selectOne(String id, Class<T> resultClass, ParamObject paramObject);

    int update(String id);

    int update(String id, ParamObject paramObject);

    int insert(String id);

    int insert(String id, ParamObject paramObject);

    <T> T insertReturnGeneratedKey(String id, Class<T> resultClass);

    <T> T insertReturnGeneratedKey(String id, Class<T> resultClass, ParamObject paramObject);

    int delete(String id);

    int delete(String id, ParamObject paramObject);


    SqlResult get(String id);

    SqlResult get(String id, ParamObject paramObject);

    /**
     * sql入参转换，例如 select * from user where username=#{username} 参数 username=lingkang
     * 输出 sql=select * from user where username=? params=[lingkang]
     *
     * @param myBatisSql  mybatis的sql语句，只能替换 #{} 的语句，例如 select * from user where username=#{username}
     * @param paramObject 入参。例如 username=lingkang
     * @return 返回 SqlResult
     * @since 4.0.0
     */
    SqlResult sqlToSqlResult(String myBatisSql, ParamObject paramObject);

    /**
     * 底层调用  finalSql.selectForList
     */
    <T> List<T> selectSqlResult(SqlResult sqlResult, Class<T> resultClass);

    /**
     * 底层调用 finalSql.nativeHumpUpdate
     */
    int updateSqlResult(SqlResult sqlResult);
}
