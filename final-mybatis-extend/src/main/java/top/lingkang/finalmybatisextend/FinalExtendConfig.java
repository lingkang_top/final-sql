package top.lingkang.finalmybatisextend;


import java.io.InputStream;

/**
 * @author lingkang
 * Created by 2022/10/10
 */
public class FinalExtendConfig {
    private boolean showSql = false;
    private InputStream[] xml;

    public FinalExtendConfig setShowSql(boolean showSql) {
        this.showSql = showSql;
        return this;
    }

    public FinalExtendConfig addXml(InputStream... xml) {
        this.xml = xml;
        return this;
    }

    public boolean isShowSql() {
        return showSql;
    }

    public InputStream[] getXml() {
        return xml;
    }

    public void setXml(InputStream[] xml) {
        this.xml = xml;
    }
}
