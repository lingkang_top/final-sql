package top.lingkang.finalmybatisextend.utils;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class AssertUtils {
    public static void notNull(Object obj,String msg){
        if (obj==null)
            throw new IllegalArgumentException(msg);
    }
}
