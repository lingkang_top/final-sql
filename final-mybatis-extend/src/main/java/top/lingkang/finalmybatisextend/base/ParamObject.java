package top.lingkang.finalmybatisextend.base;

import java.util.HashMap;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class ParamObject extends HashMap<String,Object> {
    public ParamObject add(String key, Object value) {
        put(key, value);
        return this;
    }
}
