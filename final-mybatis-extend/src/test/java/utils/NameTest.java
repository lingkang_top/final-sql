package utils;

import org.junit.jupiter.api.Test;
import top.lingkang.finalsql.utils.NameUtils;

/**
 * @author lingkang
 * Created by 2022/10/16
 */
public class NameTest {
    @Test
    public void nameUtils() {
        System.out.println(NameUtils.toHump("sys__a_user"));
        System.out.println(NameUtils.toHump("sys_user_role"));
        System.out.println(NameUtils.toHump("sys_1_role"));

        System.out.println("sysUser: " + NameUtils.unHump("sysUser"));
        System.out.println("sysUserRole: " + NameUtils.unHump("sysUserRole"));
        System.out.println("sys1Role: " + NameUtils.unHump("sys1Role"));
        System.out.println("sysUserRole: " + NameUtils.unHump("sysUserRole"));
    }
}
