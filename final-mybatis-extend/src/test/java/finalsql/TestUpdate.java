package finalsql;

import finalsql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import top.lingkang.finalmybatisextend.base.ParamObject;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class TestUpdate extends BaseMysqlTest {
    @Test
    public void test01() {
        int update01 = mybatisTemplate.update("update01", new ParamObject().add("id", 530380));
        System.out.println(update01);
    }
}
