package finalsql;

import cn.hutool.core.lang.Assert;
import finalsql.common.BaseTest;
import org.junit.jupiter.api.Test;
import top.lingkang.finalmybatisextend.base.ParamObject;
import top.lingkang.finalmybatisextend.entity.SqlResult;

import java.util.Arrays;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class TestBase extends BaseTest {

    @Test
    void test01() {
        SqlResult sqlResult = mybatisTemplate.get(
                "selectUser",
                new ParamObject()
                        .add("id", "123")
                        .add("list", Arrays.asList(1, 2, 3, 4))
                        .add("isOpen", true));
        System.out.println("params=" + sqlResult.getParams());
        Assert.notNull(sqlResult, "");
    }

    @Test
    void test02() {
        SqlResult sqlResult = mybatisTemplate.get(
                "selectUser",
                new ParamObject());
        System.out.println("params=" + sqlResult.getParams());
        Assert.notNull(sqlResult, "");
    }

}
