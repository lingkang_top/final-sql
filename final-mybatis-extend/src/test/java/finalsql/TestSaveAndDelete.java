package finalsql;

import finalsql.common.BaseMysqlTest;
import org.junit.jupiter.api.Test;
import top.lingkang.finalmybatisextend.base.ParamObject;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class TestSaveAndDelete extends BaseMysqlTest {

    @Test
    public void test01() {
        int insert01 = mybatisTemplate.insert("insert01");
        System.out.println(insert01);

        int delete01 = mybatisTemplate.delete("delete01", new ParamObject().add("id", 99999));
        System.out.println(delete01);
    }

    @Test
    public void test02() {
        Integer integer = mybatisTemplate.insertReturnGeneratedKey("insert02", Integer.class);
        System.out.println(integer);
    }
}
