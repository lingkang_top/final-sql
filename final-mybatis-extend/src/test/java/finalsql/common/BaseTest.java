package finalsql.common;

import top.lingkang.finalmybatisextend.FinalExtendConfig;
import top.lingkang.finalmybatisextend.FinalMybatisTemplate;
import top.lingkang.finalmybatisextend.MybatisTemplate;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class BaseTest {
    protected MybatisTemplate mybatisTemplate;

    public BaseTest() {
        FinalExtendConfig finalExtendConfig = new FinalExtendConfig();
        finalExtendConfig.setShowSql(true);
        finalExtendConfig.addXml(
                getClass().getClassLoader().getResourceAsStream("testMapper.xml")
        );
        mybatisTemplate = new FinalMybatisTemplate(finalExtendConfig);
    }

}
