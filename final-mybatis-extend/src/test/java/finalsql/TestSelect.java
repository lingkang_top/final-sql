package finalsql;

import finalsql.common.BaseMysqlTest;
import finalsql.entity.UserAuto;
import org.junit.jupiter.api.Test;
import top.lingkang.finalmybatisextend.FinalExtendConfig;
import top.lingkang.finalmybatisextend.FinalMybatisTemplate;
import top.lingkang.finalmybatisextend.base.ParamObject;
import top.lingkang.finalmybatisextend.entity.SqlResult;

import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2022/10/11
 */
public class TestSelect extends BaseMysqlTest {
    @Test
    public void test99() {
        FinalExtendConfig finalExtendConfig = new FinalExtendConfig();
        finalExtendConfig.setShowSql(true);
        finalExtendConfig.addXml(
                getClass().getClassLoader().getResourceAsStream("finalsqlMapper.xml"),
                getClass().getClassLoader().getResourceAsStream("testMapper.xml")
        );

        // 一定要应用上 finalSql 否则空指针
        mybatisTemplate = new FinalMybatisTemplate(finalExtendConfig).apply(finalSql);

        List<UserAuto> selectUser = mybatisTemplate.select("selectUser", UserAuto.class);
        System.out.println(selectUser);

        selectUser = mybatisTemplate.select("selectUser", UserAuto.class, new ParamObject().add("id", 530375));
        System.out.println(selectUser);
    }


    @Test
    public void test01() {
        List<UserAuto> selectUser = mybatisTemplate.select("select01", UserAuto.class);
    }

    @Test
    public void test02() {
        List<UserAuto> selectUser = mybatisTemplate.select("select02", UserAuto.class, new ParamObject().add("id", 530376));
    }

    @Test
    public void test03() {
        UserAuto selectUser = mybatisTemplate.selectOne("select03", UserAuto.class, new ParamObject().add("id", 530376));
    }

    @Test
    void test04() {
        SqlResult sqlResult = mybatisTemplate.sqlToSqlResult(
                "select * from user where username=#{username}",
                new ParamObject().add("username", "lingkang1"));
        System.out.println(mybatisTemplate.selectSqlResult(sqlResult, Map.class));
    }

}
