package finalsql.entity;

import lombok.Getter;
import lombok.Setter;
import top.lingkang.finalsql.annotation.Column;
import top.lingkang.finalsql.annotation.Id;
import top.lingkang.finalsql.annotation.Table;

import java.io.Serializable;

/**
 * @author lingkang
 * Created by 2022/9/22
 */
@Getter
@Setter
@Table("f_file")
public class FileByte implements Serializable {
    @Id
    private Integer id;
    @Column
    private String name;
    @Column
    private byte[] content;

    // 其他无关字段
    private String status;
}

