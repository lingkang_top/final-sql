package finalsql.entity;

import lombok.Data;
import top.lingkang.finalsql.annotation.Column;
import top.lingkang.finalsql.annotation.Id;
import top.lingkang.finalsql.annotation.Table;
import top.lingkang.finalsql.constants.IdType;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2022/9/18
 */
@Data
@Table("user")
public class UserInput {
    @Id(value = IdType.INPUT)
    @Column
    private Integer id;
    @Column
    private Integer num;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private Date createTime;

    // 其他无关字段
    private String status;
}
