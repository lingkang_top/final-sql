DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `num`         int(11) DEFAULT NULL,
    `username`    varchar(255) DEFAULT NULL COMMENT '用户名',
    `password`    varchar(255) DEFAULT NULL,
    `create_time` datetime     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 568278
  DEFAULT CHARSET = utf8;

INSERT INTO `user` VALUES (530368, 0, '4399', 'pwd0', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530369, 1, 'lingkang1', 'pwd1', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530370, 2, 'lingkang2', 'pwd2', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530371, 3, 'lingkang3', 'pwd3', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530372, 4, 'lingkang4', 'pwd4', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530373, 5, 'lingkang5', 'pwd5', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530374, 6, 'lingkang6', 'pwd6', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530375, 7, 'lingkang7', 'pwd7', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530376, 8, 'lingkang8', 'pwd8', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530377, 9, 'lingkang9', 'pwd9', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530378, 10, 'lingkang10', 'pwd10', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530379, 11, 'lingkang11', 'pwd11', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530380, 12, 'lingkang12', 'pwd12', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (530381, 13, 'lingkang13', 'pwd13', '2022-04-16 18:14:24');
INSERT INTO `user` VALUES (568275, NULL, 'lingkang0', NULL, '2022-04-18 00:00:00');
INSERT INTO `user` VALUES (568276, NULL, 'lingkang', '123456', '2022-05-03 13:00:29');
INSERT INTO `user` VALUES (568277, NULL, 'lingkang123', '123456123123', '2022-05-03 21:21:20');


DROP TABLE IF EXISTS `f_file`;
CREATE TABLE `f_file`
(
    `id`      int(11) NOT NULL AUTO_INCREMENT,
    `name`    varchar(255) DEFAULT NULL,
    `content` longblob,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;