# final-sql

## 介绍

* final-sql 一个轻量级数据库ORM框架。开箱即用，约定大于配置。
* final-sql 大小不到 **100 kb** ，不依赖除日志slf4j以外的**轻量级框架**。
* final-sql 兼顾hibernate表映射、mybatis动态sql、简单易用接口

## 适用场景

* 量级快速开发应用，常用增删查改等的场景。
* 嵌入应用，例如javaGUI、Android sqlite。
* 单元测试覆盖

## 快速开始
引入依赖
```xml
<!-- 已上传Maven公共仓库 -->
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>final-sql-core</artifactId>
    <version>x.y.z</version>
</dependency>
```
`x.y.z` 请选择最新的发行版本，maven公共仓库：[https://mvnrepository.com/artifact/top.lingkang/final-sql](https://mvnrepository.com/artifact/top.lingkang/final-sql)

## 文档

文档地址：[https://gitee.com/lingkang_top/final-sql/wikis](https://gitee.com/lingkang_top/final-sql/wikis)

## 起源

某天想开发一个bbs社区后台，选择ORM时，用mybatis配置多，hibernate又太重7.0+MB，想用社区某个框架又有bug，于是自己手撸一个轻量级ORM：轻量级、快速上手、配置少。
<br><br>
[wiki: nature 性能对比hibernate、mybatis](https://gitee.com/lingkang_top/final-sql/wikis/nature%20%E6%80%A7%E8%83%BD%E5%AF%B9%E6%AF%94hibernate%E3%80%81mybatis)
